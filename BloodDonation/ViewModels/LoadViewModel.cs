﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.ViewModels
{
    public class LoadViewModel
    {
        [Required]
        public List<IFormFile> CsvFiles { get; set; }
    }
}
