﻿using BloodDonation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.ViewModels
{
    public class DonorViewModel
    {

        public double Id { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
        public string BloodType { get; set; }
        public List<Donation> Donations { get; set; }
        public int BloodAmount { get; set; }

        public DonorViewModel()
        {
            Donations = new List<Donation>();
        }

    }
}
