﻿using BloodDonation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Extensions
{
    public interface ICsv
    {
        List<DonorCsvModel> csvImport(List<String> paths);
        List<string> FailToView { get; set; } 
        List<string> SuccesToView { get; set; } 
    }
}
