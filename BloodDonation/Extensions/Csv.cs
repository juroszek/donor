﻿using AutoMapper;
using BloodDonation.Data;
using BloodDonation.Models;
using CsvHelper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodDonation.Extensions
{
    public class Csv : ICsv
    {
        private readonly IHostingEnvironment _host;
        private IDonorData _data;
        private readonly IMapper _mapper;

        public List<string> FailToView { get; set; } = new List<string>();
        public List<string> SuccesToView { get; set; } = new List<string>();

        public Csv(IHostingEnvironment host, IDonorData data, IMapper mapper)
        {
            _host = host;
            _data = data;
            _mapper = mapper;
        }

        public List<DonorCsvModel> csvImport(List<String> paths)
        {
            List<DonorCsvModel> donorsCSVData = new List<DonorCsvModel>();
            FailToView = new List<string>();
            SuccesToView = new List<string>();

            foreach (string loadedCsv in paths)
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var reader = new StreamReader(_host.WebRootPath + loadedCsv, Encoding.Default))
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ",";
                    csv.Configuration.MissingFieldFound = null;

                    var recordsCsv = csv.GetRecords<DonorCsvModel>();
                    var validator = new DonorCsvValidator();

                    List<IList<ValidationFailure>> failed = new List<IList<ValidationFailure>>();


                    List<string> peselFail = new List<string>();

                    foreach (var donor in recordsCsv)
                    {
                        var results = validator.Validate(donor);
                        if (!results.IsValid)
                        {
                            IList<ValidationFailure> failures = results.Errors;
                            failed.Add(failures);
                            peselFail.Add(donor.PESEL);
                        }
                        else
                        {
                            donorsCSVData.Add(donor);
                        }
                    }

                    if (failed.Count > 0)
                    {

                        FailToView.Add($"File:{loadedCsv} Errors on records with PESEL:");
                        foreach (var pesel in peselFail)
                        {
                            FailToView.Add($"{pesel}");
                        }
                        foreach (var failure in failed)
                        {
                            foreach (var fail in failure)
                            {
                                FailToView.Add($"Property Name = {fail.PropertyName},");
                                FailToView.Add($"Attempted Value = {fail.AttemptedValue}");
                                FailToView.Add($"Message = {fail.ErrorMessage}");
                            }
                        }
                    }
                    else
                    {
                        SuccesToView.Add($"File:{loadedCsv} added to DataBase!");
                        var mappedDonors = _mapper.Map<List<DonorCsvModel>, List<Donor>>(donorsCSVData);
                        foreach (var mappedDonor in mappedDonors)
                        {
                            if (_data.GetAllDonors().Where(d => d.PESEL == mappedDonor.PESEL).Count() == 0)
                            {
                                _data.SaveDonor(mappedDonor);
                            }
                        }

                        foreach (var record in donorsCSVData)
                        {
                            var mappedDonation = _mapper.Map<DonorCsvModel, Donation>(record);
                            Donor donor = _data.GetDonorByPesel(record.PESEL);
                            mappedDonation.Donor = donor;
                            if (_data.GetAllDonations().Where(d => d.Donor == mappedDonation.Donor && d.DonationDate == mappedDonation.DonationDate).Count() == 0)
                            {
                                _data.SaveDonation(mappedDonation);
                            }

                        }

                    }
                }
            }
            return donorsCSVData;
        }
    }
}
