﻿using ChartJSCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Extensions
{
    public interface ICharts
    {
         Chart getBarChart(List<double> inputDataset, List<string> inputLabels);
    }
}
