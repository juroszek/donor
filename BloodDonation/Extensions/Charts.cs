﻿using ChartJSCore.Models;
using ChartJSCore.Models.Bar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Extensions
{
    public class Charts : ICharts
    {
        public Chart getBarChart(List<double> inputDataset, List<string> inputLabels)
        {
            Chart chart = new Chart();

            chart.Type = Enums.ChartType.Bar;

            ChartJSCore.Models.Data data = new ChartJSCore.Models.Data();

            data.Labels = inputLabels;

            BarDataset dataset = new BarDataset()
            {
                Label = "Grupa Krwi",
                Data = inputDataset,
                BackgroundColor = new List<string>()
                {
                "#400000", "#650000", "#7F0000", "#9B0000", "#BF0000", "#E60000", "#D90404", "#E60000"
                },
                BorderColor = new List<string>()
                {
                "#400000", "#650000", "#7F0000", "#9B0000", "#BF0000", "#E60000", "#D90404", "#E60000"
                },
                BorderWidth = new List<int>() { 1 }
            };

            data.Datasets = new List<Dataset>();
            data.Datasets.Add(dataset);

            chart.Data = data;

            BarOptions options = new BarOptions()
            {
                Scales = new Scales(),
                BarPercentage = 0.7
            };

            Scales scales = new Scales()
            {
                YAxes = new List<Scale>()
                {
                    new CartesianScale()
                    {
                        Ticks = new CartesianLinearTick()
                        {
                            BeginAtZero = true
                        }
                    }
                }
            };

            options.Scales = scales;

            chart.Options = options;

            chart.Options.Layout = new Layout()
            {
                Padding = new Padding()
                {
                    PaddingObject = new PaddingObject()
                    {
                        Left = 10,
                        Right = 12
                    }
                }
            };

            return chart;
        }
    
    }
}
