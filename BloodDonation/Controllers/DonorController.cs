﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BloodDonation.Data;
using BloodDonation.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using BloodDonation.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Http;
using BloodDonation.Extensions;

namespace BloodDonation.Controllers
{

    public class DonorController : Controller
    {
        public static List<DonorCsvModel> DonorsCSVData { get; set; } = new List<DonorCsvModel>();

        private IDonorData _data;
        private readonly IHostingEnvironment _host;
        private readonly IMapper _mapper;
        private readonly ICharts _charts;
        private readonly ICsv _csv;

        public DonorController(IHostingEnvironment host, IMapper mapper, IDonorData data, ICharts charts, ICsv csv)
        {
            _host = host;
            _data = data;
            _mapper = mapper;
            _charts = charts;
            _csv = csv;
        }

        public ActionResult Index()
        {

            var donors = _data.GetAllDonors().ToList();
            var donations = _data.GetAllDonations().ToList();

            foreach (var donor in donors)
            {
                donor.Donations = donations.Where(d => d.Donor == donor).ToList();
            }

            var viewDonors = _mapper.Map<List<Donor>, List<DonorViewModel>>(donors);
            var representations = new List<int>();

            var distDonors = viewDonors.Select(s => s.BloodType).ToList().Distinct();

            foreach (var item in distDonors)
            {
                representations.Add(viewDonors.Count(x => x.BloodType == item));
            }

            List<double> rep = representations.Select<int, double>(i => i).ToList();

            var dataLabels = distDonors.ToList<String>();
            ViewData["chart"] = _charts.getBarChart(rep, dataLabels);

            return View(viewDonors);
        }

        [HttpGet]
        public ActionResult Load()
        {
            return View();
        }


        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public ActionResult Load(LoadViewModel loadViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string folderName = "csv";
                    string webRootPath = _host.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);

                    List<string> paths = new List<string>();

                    foreach (IFormFile csv in loadViewModel.CsvFiles)
                    {
                        string fileName = csv.FileName;
                        var savePath = Path.Combine(newPath, fileName);
                        using (var csvFile = new FileStream(savePath, FileMode.Create))
                        {
                            csv.CopyTo(csvFile);
                        }
                        paths.Add("/" + folderName + "/" + fileName);
                    }

                    DonorsCSVData = _csv.csvImport(paths);
                    if (_csv.SuccesToView.Count != 0)
                    {
                        ViewBag.succes = _csv.SuccesToView;
                    }
                    if (_csv.FailToView.Count != 0)
                    {
                        ViewBag.error = _csv.FailToView;
                    }
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

    }
}