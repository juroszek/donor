﻿using BloodDonation.Models;
using Microsoft.EntityFrameworkCore;

namespace BloodDonation.Data
{
    public class DonorDbContext : DbContext
    {

        public DonorDbContext(DbContextOptions<DonorDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Donor>()
                .HasMany(d => d.Donations)
                .WithOne(d => d.Donor)
                .IsRequired();
        }

        public DbSet<Donor> Donors { get; set; }
        public DbSet <Donation> Donations { get; set; }

    }
}