﻿using BloodDonation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Data
{
    public interface IDonorData
    {
        void SaveDonor(Donor donor);
        void SaveDonation(Donation donation);
        IEnumerable<Donor> GetAllDonors();
        IEnumerable<Donation> GetAllDonations();
        Donor GetDonorByPesel(string pesel);
    }
}
