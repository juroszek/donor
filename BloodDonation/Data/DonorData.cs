﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BloodDonation.Models;
using CsvHelper;
using Microsoft.AspNetCore.Hosting;

namespace BloodDonation.Data
{
    public class DonorData : IDonorData
    {

        private readonly DonorDbContext _data;

        public DonorData(DonorDbContext context)
        {
            _data = context;
        }

        public Donor GetDonorByPesel(string pesel)
        {
            return _data.Donors.Where(d => d.PESEL == pesel).FirstOrDefault();
        }

        public void SaveDonor(Donor donor)
        {
            _data.Donors.Add(donor);
            _data.SaveChanges();
        }

        public void SaveDonation(Donation donation)
        {
            _data.Donations.Add(donation);
            _data.SaveChanges();
        }

        public IEnumerable<Donor> GetAllDonors()
        {
            return _data.Donors.ToList();
        }

        public IEnumerable<Donation> GetAllDonations()
        {
            return _data.Donations.ToList();
        }
    }
}
