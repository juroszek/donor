﻿using AutoMapper;
using BloodDonation.Models;
using BloodDonation.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Data
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<DonorCsvModel, Donor>()
                .ForMember(dest => dest.PESEL, opt => opt.MapFrom(src => src.PESEL))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.BloodType, opt => opt.MapFrom(src => src.BloodType))
                .ForMember(dest => dest.Rh, opt => opt.MapFrom(src => src.Rh));

            CreateMap<DonorCsvModel, Donation>()
                .ForMember(dest => dest.DonationDate, opt => opt.MapFrom(src => src.DonationDate))
                .ForMember(dest => dest.BloodAmount, opt => opt.MapFrom(src => src.BloodAmount));

            CreateMap<Donor, DonorViewModel>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => string.Join(" ", src.FirstName, src.LastName)))
                .ForMember(dest => dest.BloodType, opt => opt.MapFrom(src => string.Join(" ", src.BloodType, src.Rh)))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PESEL))
                .ForMember(dest => dest.Donations, opt => opt.MapFrom(src => src.Donations))
                .ForMember(dest => dest.BloodAmount, opts => opts.MapFrom(src => src.Donations.Select(d => d.BloodAmount).Sum()));
        }

    }
}
