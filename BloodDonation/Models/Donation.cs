﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Models
{
    public class Donation
    {
        public Guid Id { get; set; }
        public Donor Donor { get; set; }
        public DateTime DonationDate { get; set; }
        public int BloodAmount { get; set; }

        public Donation()
        {
            Id = Guid.NewGuid();
        }
    }

}

