﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Models
{
    public class DonorCsvModel
    {
        public string PESEL { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string DonationDate { get; set; }
        public string BloodAmount { get; set; }
        public string BloodType { get; set; }
        public string Rh { get; set; }
    }
}
