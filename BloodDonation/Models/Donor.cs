﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonation.Models
{
    public class Donor
    {
        public Guid Id { get; set; }
        public string PESEL { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string BloodType { get; set; }
        public string Rh { get; set; }
        public List<Donation> Donations { get; set; }

        public Donor()
        {
            Donations = new List<Donation>();
            Id = Guid.NewGuid();
        }
    }

}
