﻿using FluentValidation;
using System.Collections.Generic;

namespace BloodDonation.Models
{
    public class DonorCsvValidator : AbstractValidator<DonorCsvModel>
    {
        public DonorCsvValidator()
        {
            List<string> BloodTypeValues = new List<string> { "0", "A", "B", "AB" };
            List<string> BloodFactorValues = new List<string> { "Rh+", "Rh-" };

            RuleFor(x => x.PESEL).NotNull().Matches(@"^\d{11}$");
            RuleFor(x => x.FirstName).Length(0, 10);
            RuleFor(x => x.LastName).NotNull().Length(0, 20);
            RuleFor(x => x.Address).NotNull().Length(0, 30);
            RuleFor(x => x.DonationDate).NotNull();
            RuleFor(x => x.BloodAmount).NotNull().ExclusiveBetween("0", "451");
            RuleFor(x => x.BloodType).NotNull().Must(x => BloodTypeValues.Contains(x));
            RuleFor(x => x.Rh).NotNull().Must(x => BloodFactorValues.Contains(x));
        }

    }
}
